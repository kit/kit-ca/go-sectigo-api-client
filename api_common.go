package sectigoapiclient

const (
	SECTIGOAPIURL = "https://cert-manager.com"
)

//go:generate stringer -type=CertificateState -trimprefix=CertificateState -output zzz_CertificateState_string.go
type CertificateState int

const (
	CertificateStateBlank CertificateState = 1 + iota
	CertificateStateCreated
	CertificateStateRequested
	CertificateStateIssued
	CertificateStateDownloaded
	CertificateStateExpired
	CertificateStateRevoked
	CertificateStateRejected
	CertificateStatePreRevoked
)

//go:generate stringer -type=APIRequest -trimprefix=ApiRequest -output zzz_ApiRequest_string.go
type APIRequest int

const (
	// SSL
	ApiRequestGetSSLCertificateDetails APIRequest = 1 + iota
	ApiRequestUpdateSSLCertificate
	ApiRequestListSSLCertificates
	ApiRequestCountSSLCertificates
	ApiRequestSSLCertificateProfiles
	ApiRequestListSSLCustomFields
	ApiRequestEnrollSSLCertificate
	ApiRequestCollectSSLCertificate
	ApiRequestRevokeSSLCertificateByID
	ApiRequestRevokeSSLCertificateBySerial
	ApiRequestApproveSSLCertificate
	ApiRequestDeclineSSLCertificate
	// Client (incomplete)
	ApiRequestClientCertificateTypes
	ApiRequestEnrollClientCertificate
	ApiRequestCollectClientCertificate
	ApiRequestListClientCertificates
	ApiRequestCountClientCertificates
	ApiRequestListClientCertificateByEmail
	ApiRequestListClientCertificateByPersonID
	// DCV
	ApiRequestDCVStartHTTP
	ApiRequestDCVStartHTTPS
	ApiRequestDCVStartCName
	ApiRequestDCVStartEMail
	ApiRequestDCVSubmitValidationHTTP
	ApiRequestDCVSubmitValidationHTTPS
	ApiRequestDCVSubmitValidationCNAME
	ApiRequestDCVSubmitValidationEMail
	ApiRequestDCVGetValidationStatus
	ApiRequestDCVSearchDomains
	ApiRequestDCVClearValidation
	// Domain
	APIRequestDomainCreateNew
	APIRequestDomainDelete
	APIRequestDomainGetInfo
	APIRequestDomainGetList
	APIRequestDomainCount
	APIRequestDomainActivate
	APIRequestDomainSuspend
	APIRequestDomainDelegate
	APIRequestDomainRemoveDelegation
	APIRequestDomainApproveDelegation
	APIRequestDomainRejectDelegation
	// Reports
	ApiRequestReportClientCertificates
)

//go:generate stringer -type=ResponseLocation -trimprefix=ResponseLocation -output zzz_ResponseLocation_string.go
type ResponseLocation int

const (
	ResponseLocationBody ResponseLocation = 1 + iota
	ResponseLocationHeader
	ResponseLocationBoth
	ResponseLocationReturnCode
)

type APIRequestConfig struct {
	Verb             string
	PathPrefix       string
	HasArgument      bool
	ResponseLocation ResponseLocation
}

var ApiRequests = map[APIRequest]APIRequestConfig{
	ApiRequestGetSSLCertificateDetails: {
		Verb:             "GET",
		PathPrefix:       "/api/ssl/v1/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestUpdateSSLCertificate: {
		Verb:             "PUT",
		PathPrefix:       "/api/ssl/v1",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestListSSLCertificates: {
		Verb:             "GET",
		PathPrefix:       "/api/ssl/v1",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBoth,
	},
	ApiRequestCountSSLCertificates: {
		Verb:             "HEAD",
		PathPrefix:       "/api/ssl/v1",
		HasArgument:      false,
		ResponseLocation: ResponseLocationHeader,
	},
	ApiRequestEnrollSSLCertificate: {
		Verb:             "POST",
		PathPrefix:       "/api/ssl/v1/enroll",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestSSLCertificateProfiles: {
		Verb:             "GET",
		PathPrefix:       "/api/ssl/v1/types?organizationId=",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestCollectSSLCertificate: {
		Verb:             "GET",
		PathPrefix:       "/api/ssl/v1/collect/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestRevokeSSLCertificateByID: {
		Verb:             "POST",
		PathPrefix:       "/api/ssl/v1/revoke/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	ApiRequestRevokeSSLCertificateBySerial: {
		Verb:             "POST",
		PathPrefix:       "/api/ssl/v1/revoke/serial/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	ApiRequestApproveSSLCertificate: {
		Verb:             "POST",
		PathPrefix:       "/api/ssl/v1/approve/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationHeader,
	},
	ApiRequestDeclineSSLCertificate: {
		Verb:             "POST",
		PathPrefix:       "/api/ssl/v1/decline/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	ApiRequestListSSLCustomFields: {
		Verb:             "GET",
		PathPrefix:       "/api/ssl/v1/customFields",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestClientCertificateTypes: {
		Verb:             "GET",
		PathPrefix:       "/api/smime/v1/types",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestEnrollClientCertificate: {
		Verb:             "POST",
		PathPrefix:       "/api/smime/v1/enroll",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestCollectClientCertificate: {
		Verb:             "GET",
		PathPrefix:       "/api/smime/v1/collect/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBoth,
	},
	ApiRequestListClientCertificates: {
		Verb:             "GET",
		PathPrefix:       "/api/smime/v2",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBoth,
	},
	ApiRequestCountClientCertificates: {
		Verb:             "HEAD",
		PathPrefix:       "/api/smime/v2",
		HasArgument:      false,
		ResponseLocation: ResponseLocationHeader,
	},
	ApiRequestListClientCertificateByEmail: {
		Verb:             "GET",
		PathPrefix:       "/api/smime/v2/byPersonEmail/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestListClientCertificateByPersonID: {
		Verb:             "GET",
		PathPrefix:       "/smime/v2/byPersonId/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVStartHTTP: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/start/domain/http",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVStartHTTPS: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/start/domain/https",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVStartCName: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/start/domain/cname",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVStartEMail: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/start/domain/email",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVSubmitValidationHTTP: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/submit/domain/http",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVSubmitValidationHTTPS: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/submit/domain/https",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVSubmitValidationCNAME: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/submit/domain/cname",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVSubmitValidationEMail: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/submit/domain/email",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVGetValidationStatus: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v2/validation/status",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVSearchDomains: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	ApiRequestDCVClearValidation: {
		Verb:             "POST",
		PathPrefix:       "/api/dcv/v1/validation/clear",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	APIRequestDomainCreateNew: {
		Verb:             "POST",
		PathPrefix:       "/api/domain/v1",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
	APIRequestDomainDelete: {
		Verb:             "DELETE",
		PathPrefix:       "/api/domain/v1/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	APIRequestDomainGetInfo: {
		Verb:             "GET",
		PathPrefix:       "/api/domain/v1/",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	APIRequestDomainGetList: {
		Verb:             "GET",
		PathPrefix:       "/api/domain/v1",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	APIRequestDomainCount: {
		Verb:             "GET",
		PathPrefix:       "/api/domain/v1/count",
		HasArgument:      true,
		ResponseLocation: ResponseLocationBody,
	},
	APIRequestDomainActivate: {
		Verb:             "PUT",
		PathPrefix:       "/api/domain/v1/%d/activate", // TODO: fuuuuu
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	APIRequestDomainSuspend: {
		Verb:             "PUT",
		PathPrefix:       "/api/domain/v1/%d/suspend", // TODO: fuuuuu
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	APIRequestDomainDelegate: {
		Verb:             "POST",
		PathPrefix:       "/api/domain/v1/%d/delegation", // TODO: fuuuuu
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	APIRequestDomainRemoveDelegation: {
		Verb:             "DELETE",
		PathPrefix:       "/api/domain/v1/%s/delegation", // TODO: fuuuuu
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	APIRequestDomainApproveDelegation: {
		Verb:             "POST",
		PathPrefix:       "/api/domain/v1/%d/delegation/approve", // TODO: fuuuuu
		HasArgument:      true,
		ResponseLocation: ResponseLocationReturnCode,
	},
	APIRequestDomainRejectDelegation: {
		Verb:             "POST",
		PathPrefix:       "/api/domain/v1/%d/delegation/reject",
		HasArgument:      false,
		ResponseLocation: ResponseLocationReturnCode,
	},
	ApiRequestReportClientCertificates: {
		Verb:             "POST",
		PathPrefix:       "/api/report/v1/client-certificates",
		HasArgument:      false,
		ResponseLocation: ResponseLocationBody,
	},
}
