package sectigoapiclient

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
)

type DomainCount struct {
	Count int `json:"count"`
}

func (c *SectigoClient) CountDomains() (count int, err error) {
	var (
		countResp   *http.Response
		countStruct DomainCount
	)
	// perform API request
	countResp, err = c.doRequest(APIRequestDomainCount, "", nil)
	if err != nil {
		return -1, err
	}

	// parse response body
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(countResp.Body)
	if err != nil {
		return -1, err
	}
	err = json.Unmarshal(buf.Bytes(), &countStruct)
	if err != nil {
		return -1, err
	}
	return countStruct.Count, nil
}

type DomainsListing []struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (c *SectigoClient) ListDomains() (domains DomainsListing, err error) {
	domains = DomainsListing{}

	// count domains
	count, err := c.CountDomains()
	if err != nil {
		return nil, err
	}

	for len(domains) < count {
		var (
			d       = DomainsListing{}
			getArgs = url.Values{}
		)
		// perform API request
		getArgs.Set("position", strconv.Itoa(len(domains)))
		requestParams := `?` + getArgs.Encode() // TODO: this is ugly
		resp, err := c.doRequest(APIRequestDomainGetList, requestParams, nil)
		if err != nil {
			return nil, err
		}

		// parse response body
		buf := new(bytes.Buffer)
		_, err = buf.ReadFrom(resp.Body)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(buf.Bytes(), &d)
		if err != nil {
			return nil, err
		}

		// append to domains
		domains = append(domains, d...)
	}
	return domains, nil
}

type DomainCertTypes struct {
	SSL, SMIME, CodeSign bool
}

func (dct DomainCertTypes) MarshalJSON() ([]byte, error) {
	var r []string
	if dct.SSL {
		r = append(r, "SSL")
	}
	if dct.SMIME {
		r = append(r, "SMIME")
	}
	if dct.CodeSign {
		r = append(r, "CodeSign")
	}
	return json.Marshal(r)
}

type NewDomainRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Active      bool   `json:"active"`
	Delegations []struct {
		OrgID     int             `json:"orgId,omitempty"`
		CertTypes DomainCertTypes `json:"certTypes"`
	} `json:"delegations"`
}
