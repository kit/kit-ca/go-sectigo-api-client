package sectigoapiclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strconv"
)

type CertificateListing []struct {
	SSLID                   int
	CommonName              string
	SubjectAlternativeNames []string
	SerialNumber            string
}

// ListSSLCertificates lists all certificates that match filter.
// Be advised: these listings are not complete; implement paging!
func (c *SectigoClient) ListSSLCertificates(filter CommonCertificateFilter) (listing CertificateListing, count int, err error) {
	// perform API request
	resp, err := c.doRequest(ApiRequestListSSLCertificates, "?"+filter.AsURLParameterString(), nil)
	if err != nil {
		return nil, -1, err
	}

	// parse response body
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, -1, err
	}
	err = json.Unmarshal(buf.Bytes(), &listing)
	if err != nil {
		return nil, -1, err
	}

	// read header
	var totalCount int
	totalCount, err = strconv.Atoi(resp.Header.Get("X-Total-Count"))
	if err != nil {
		return listing, 0, nil
	}
	return listing, totalCount, nil
}

// CountSSLCertificates returns the number of certificates that adhere to filter
func (c *SectigoClient) CountSSLCertificates(filter CommonCertificateFilter) (count int, err error) {
	count = -1

	resp, err := c.doRequest(ApiRequestCountSSLCertificates, "?"+filter.AsURLParameterString(), nil)
	if err != nil {
		return count, err
	}

	counthdr := resp.Header.Get("X-Total-Count")
	if len(counthdr) == 0 {
		return 0, errors.New("no count header found")
	}

	count, err = strconv.Atoi(counthdr)
	return count, err
}

type SSLKeyTypes map[string][]string

type CertificateDetails struct {
	CommonName    string
	SSLID         int
	ID            int
	Status        string
	OrderNumber   int
	BackendCertId string
	CertType      struct {
		ID                  int
		UseSecondaryOrgName bool
		Name                string
		Description         string
		Terms               []int
		KeyTypes            SSLKeyTypes
	}
	Term                    int
	Vendor                  string
	Owner                   string
	Requester               string
	Comments                string
	Requested               string
	Approved                string
	Expires                 string
	Renewed                 bool
	KeyAlgorithm            string
	KeySize                 int
	KeyType                 string
	SubjectAlternativeNames []string
	SerialNumber            string
	CustomFields            []struct {
		Name  string
		Value string
	}
	CertificateDetails struct {
		Issuer          string
		Subject         string
		SubjectAltNames string
		MD5Hash         string
		SHA1Hash        string
	}
}

// GetSSLCertificateDetails returns all details for a single certificate
func (c *SectigoClient) GetSSLCertificateDetails(certID int) (details *CertificateDetails, err error) {
	resp, err := c.doRequest(ApiRequestGetSSLCertificateDetails, strconv.Itoa(certID), nil)
	if err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(buf.Bytes(), &details)
	if err != nil {
		return nil, err
	}
	return details, nil
}

type SSLCertificateProfile struct {
	ID                  int         `json:"id"`
	Name                string      `json:"name"`
	UseSecondaryOrgName bool        `json:"useSecondaryOrgName"`
	Description         string      `json:"description"`
	Terms               []int       `json:"terms"`
	KeyTypes            SSLKeyTypes `json:"keyTypes"`
}

type SSLCertificateProfiles []SSLCertificateProfile

func (p *SSLCertificateProfiles) GetProfile(name string) (profile SSLCertificateProfile, exists bool) {
	for _, prof := range *p {
		if prof.Name == name {
			return prof, true
		}
	}
	return profile, false
}

// ListCertificateProfiles lists all available SSL certificate profiles
func (c *SectigoClient) ListCertificateProfiles(orgID int) (profiles *SSLCertificateProfiles, err error) {
	resp, err := c.doRequest(ApiRequestSSLCertificateProfiles, strconv.Itoa(orgID), nil)
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(buf.Bytes(), &profiles)
	if err != nil {
		return nil, err
	}
	return profiles, nil
}

const (
	CertificateFormatX509WithChainPEM         = "x509"   // same as "pem"
	CertificateFormatX509PEM                  = "x509CO" // same as "pemco"
	CertificateFormatPKCS7PEM                 = "base64"
	CertificateFormatPKCS7                    = "bin"
	CertificateFormatRootIntermediatesOnlyPEM = "x509IO"
	CertificateFormatIntermediatesRootOnlyPEM = "x509IOR"
	CertificateFormatX509WithChainPEM2        = "pem"
	CertificateFormatX509CertonlyPEM          = "pemco"
	CertificateFormatX509WithIssuerPEM        = "pemia"
	CertificateFormatPKCS12                   = "pkcs12"
)

var (
	CollectSSLCertificatePublicKeyList = []string{
		CertificateFormatX509WithChainPEM,
		CertificateFormatX509PEM,
		CertificateFormatPKCS7PEM,
		CertificateFormatPKCS7,
		CertificateFormatRootIntermediatesOnlyPEM,
		CertificateFormatIntermediatesRootOnlyPEM,
		CertificateFormatX509WithChainPEM2,
		CertificateFormatX509CertonlyPEM,
		CertificateFormatX509WithIssuerPEM,
	}
)

// CollectSSLCertificate retrieves a certificate plus the chain (optional)
func (c *SectigoClient) CollectSSLCertificate(certID int, format string) ([]byte, error) {
	resp, err := c.doRequest(ApiRequestCollectSSLCertificate, fmt.Sprintf("%d?format=%s", certID, url.QueryEscape(format)), nil)
	if err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

type SSLCustomFieldsResponse []struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Mandatory bool   `json:"mandatory"`
}

func (c *SectigoClient) ListSSLCustomFields() (customfields SSLCustomFieldsResponse, err error) {
	resp, err := c.doRequest(ApiRequestListSSLCustomFields, "", nil)
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(buf.Bytes(), &customfields)
	if err != nil {
		return nil, err
	}
	return customfields, nil
}
