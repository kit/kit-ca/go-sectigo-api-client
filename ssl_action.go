package sectigoapiclient

import (
	"bytes"
	"encoding/json"
	"io"
	"strconv"
)

type EnrollSSLCertificateRequest struct {
	OrgID        int    `json:"orgId,omitempty"`
	CSR          string `json:"csr,omitempty"`
	SubjAltNames string `json:"subjAltNames,omitempty"`
	CertType     int    `json:"certType,omitempty"`
	Term         int    `json:"term,omitempty"`
	Comments     string `json:"comments,omitempty"`
	CustomFields []struct {
		Name  string `json:"name,omitempty"`
		Value string `json:"value,omitempty"`
	} `json:"customFields,omitempty"`
	ExternalRequester string `json:"externalRequester,omitempty"`
}

func (f EnrollSSLCertificateRequest) Reader() (io.Reader, error) {
	jsonbuf, err := json.Marshal(f)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(jsonbuf), nil
}

type CertificateEnrollmentResponse struct {
	SSLID   int
	RenewId string
}

// EnrollSSLCertificate submits a new certificate request
func (c *SectigoClient) EnrollSSLCertificate(certRequest EnrollSSLCertificateRequest) (response *CertificateEnrollmentResponse, err error) {
	fr, err := certRequest.Reader()
	if err != nil {
		return nil, err
	}
	resp, err := c.doRequest(ApiRequestEnrollSSLCertificate, "", fr)
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

type ReasonBody struct {
	Reason string `json:"reason"`
}

type MessageBody struct {
	Message string `json:"message"`
}

func ReasonToReader(reason string) io.Reader {
	reasonBytes, err := json.Marshal(ReasonBody{Reason: reason})
	if err != nil {
		return bytes.NewReader(nil)
	}
	return bytes.NewReader(reasonBytes)
}

func MessageToReader(message string) io.Reader {
	messageBytes, err := json.Marshal(MessageBody{Message: message})
	if err != nil {
		return bytes.NewReader(nil)
	}
	return bytes.NewReader(messageBytes)
}

func (c *SectigoClient) RevokeSSLCertificateByID(sslID int, reason string) error {
	resp, err := c.doRequest(ApiRequestRevokeSSLCertificateByID, strconv.Itoa(sslID), ReasonToReader(reason))
	if resp.StatusCode == 204 {
		return nil
	}
	return err
}

func (c *SectigoClient) RevokeSSLCertificateBySerial(serial string, reason string) error {
	resp, err := c.doRequest(ApiRequestRevokeSSLCertificateBySerial, serial, ReasonToReader(reason))
	if resp.StatusCode == 204 {
		return nil
	}
	return err
}

func (c *SectigoClient) ApproveSSLCertificate(sslID int, message string) error {
	resp, err := c.doRequest(ApiRequestApproveSSLCertificate, strconv.Itoa(sslID), MessageToReader(message))
	if resp.StatusCode == 204 {
		return nil
	}
	return err
}

func (c *SectigoClient) DeclineSSLCertificate(sslID int, message string) error {
	resp, err := c.doRequest(ApiRequestDeclineSSLCertificate, strconv.Itoa(sslID), MessageToReader(message))
	if resp.StatusCode == 204 {
		return nil
	}
	return err
}

type UpdateCertificateRequest struct {
	SSLID                   int      `json:"sslId"`
	Term                    int      `json:"term,omitempty"`
	CertTypeId              int      `json:"certTypeId,omitempty"`
	OrgID                   int      `json:"orgId,omitempty"`
	CommonName              string   `json:"commonName,omitempty"`
	CSR                     string   `json:"csr,omitempty"`
	ExternalRequester       string   `json:"externalRequester,omitempty"`
	Comments                string   `json:"comments,omitempty"`
	SubjectAlternativeNames []string `json:"subjectAlternativeNames,omitempty"`
	CustomFields            []struct {
		Name  string `json:"name,omitempty"`
		Value string `json:"value,omitempty"`
	} `json:"customFields,omitempty"`
}

func (r UpdateCertificateRequest) Reader() (io.Reader, error) {
	jsonbuf, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(jsonbuf), nil
}

type UpdateCertificateResponse struct {
	CommonName    string `json:"commonName,omitempty"`
	SSLID         int    `json:"sslid"`
	ID            int    `json:"id"`
	OrgID         int    `json:"orgId,omitempty"`
	Status        string `json:"status,omitempty"`
	BackendCertId string `json:"backendCertId,omitempty"`
	CertType      struct {
		ID                  int    `json:"id,omitempty"`
		UseSecondaryOrgName bool   `json:"useSecondaryOrgName,omitempty"`
		Name                string `json:"name,omitempty"`
		Description         string `json:"description,omitempty"`
		Terms               []int  `json:"terms,omitempty"`
		KeyTypes            SSLKeyTypes
	} `json:"certType,omitempty"`
	Term                    int      `json:"term,omitempty"`
	Vendor                  string   `json:"vendor,omitempty"`
	Owner                   string   `json:"owner,omitempty"`
	Requester               string   `json:"requester,omitempty"`
	Comments                string   `json:"comments,omitempty"`
	Requested               string   `json:"requested,omitempty"`
	Approved                string   `json:"approved,omitempty"`
	Expires                 string   `json:"expires,omitempty"`
	Renewed                 bool     `json:"renewed,omitempty"`
	KeyAlgorithm            string   `json:"keyAlgorithm,omitempty"`
	KeyType                 string   `json:"keyType,omitempty"`
	SubjectAlternativeNames []string `json:"subjectAlternativeNames,omitempty"`
	CustomFields            []struct {
		Name  string `json:"name,omitempty"`
		Value string `json:"value,omitempty"`
	} `json:"customFields,omitempty"`
	CertificateDetails struct {
		Issuer          string `json:"issuer,omitempty"`
		Subject         string `json:"subject,omitempty"`
		SubjectAltNames string `json:"subjectAltNames,omitempty"`
		MD5Hash         string `json:"md5Hash,omitempty"`
		SHA1Hash        string `json:"sha1Hash,omitempty"`
	} `json:"certificateDetails,omitempty"`
}

func (c *SectigoClient) UpdateSSLCertificate(changes UpdateCertificateRequest) (response UpdateCertificateResponse, err error) {
	bodyReader, err := changes.Reader()
	if err != nil {
		return UpdateCertificateResponse{}, err
	}
	//{
	//	r, _ := changes.Reader()
	//	b, _ := ioutil.ReadAll(r)
	//	fmt.Fprintln(os.Stderr, string(b))
	//}
	resp, err := c.doRequest(ApiRequestUpdateSSLCertificate, "", bodyReader)
	if err != nil {
		return UpdateCertificateResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return UpdateCertificateResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return UpdateCertificateResponse{}, err
	}
	return response, nil
}
