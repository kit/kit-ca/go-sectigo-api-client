## Build requirements


### Install `stringer`
```
go install golang.org/x/tools/...@latest
```

### Sectigo credentials for unit tests

Copy `_sectigo_test_config.json.example` to `_sectigo_test_config.json` and fill all fields.
