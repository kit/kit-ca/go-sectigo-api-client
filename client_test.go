package sectigoapiclient

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"github.com/sanity-io/litter"
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"testing"
	"time"
)

var (
	orgID int
)

type sectigoPasswordCredentials struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type sectigoConfiguration struct {
	OrgID    int `json:"org_id"`
	Accounts struct {
		AutoApprove   sectigoPasswordCredentials `json:"auto-approve"`
		NoAutoApprove sectigoPasswordCredentials `json:"no-auto-approve"`
	} `json:"accounts"`
}

func prepareSectigoConfiguration() (clientAuto, clientNoAuto *SectigoClient, err error) {
	var config sectigoConfiguration

	contents, err := os.ReadFile("_sectigo_test_config.json")
	if err != nil {
		return nil, nil, err
	}
	err = json.Unmarshal(contents, &config)
	if err != nil {
		return nil, nil, err
	}

	orgID = config.OrgID

	getAutoCredentials := func() (string, string) {
		return config.Accounts.AutoApprove.Login, config.Accounts.AutoApprove.Password
	}
	getNoAutoCredentials := func() (string, string) {
		return config.Accounts.NoAutoApprove.Login, config.Accounts.NoAutoApprove.Password
	}

	return NewPasswordClient(getAutoCredentials, "DFN"),
		NewPasswordClient(getNoAutoCredentials, "DFN"),
		nil
}

// global state für all tests
var collectedTestData = struct {
	ExistingCertificateID                       int
	CleanupDeclineCertificateIDs                []int
	RSAProfile, ECCProfile                      SSLCertificateProfile
	ClientCertificateTypes                      ClientCertificateTypeResponse
	CleanupDeclineClientCertificateOrderNumbers []int
}{
	ExistingCertificateID:                       3449733,
	CleanupDeclineClientCertificateOrderNumbers: make([]int, 10),
}

var (
	clientAuto, clientNoAuto *SectigoClient
	skipCertificateCreation  bool
)

func init() {
	var err error
	// setup test API clients
	clientAuto, clientNoAuto, err = prepareSectigoConfiguration()
	if err != nil {
		log.Fatal(err)
	}

	// Decide if creation of real certificates should be skipped
	skipCertificateCreation = true
}

func TestServerCertificates(t *testing.T) {
	t.Run("SSL: CommonCertificateFilter", func(t *testing.T) {
		var f = NewCommonCertificateFilter().
			Size(100).
			CommonName("CN").
			OrgID(123).
			installStatus(InstallStatusSuccessful)

		assert.Equal(t, f.AsURLParameterString(), `commonName=CN&installStatus=SUCCESSFUL&orgId=123&size=100`, "filterstring matched spec")
	})
	t.Run("SSL: List certificate profiles", func(t *testing.T) {
		profiles, err := clientAuto.ListCertificateProfiles(orgID)
		assert.NoError(t, err, "ListCertificateProfiles returns no error")
		assert.True(t, len(*profiles) > 0, "profile list is not empty")
		//litter.Dump(profiles)
		if p, exists := profiles.GetProfile("OV Multi-Domain"); exists {
			collectedTestData.RSAProfile = p
		} else {
			t.Fatal("unable to find profile data for »OV Multi-Domain«")
		}
		if p, exists := profiles.GetProfile("GÉANT IGTF Multi-Domain"); exists {
			collectedTestData.ECCProfile = p
		} else {
			t.Fatal("unable to find profile data for »OV Multi-Domain«")
		}
		//litter.Dump(profiles)
	})
	t.Run("SSL: List custom fields for SSL", func(t *testing.T) {
		resp, err := clientAuto.ListSSLCustomFields()
		assert.NoError(t, err, "ListSSLCustomFields returns no error")
		assert.IsType(t, SSLCustomFieldsResponse{}, resp, "dummy placeholder test")
	})
	t.Run("SSL: Count Certificates (no filter)", func(t *testing.T) {
		count, err := clientAuto.CountSSLCertificates(EmptyCertificateFilter)
		assert.NoError(t, err, "CountSSLCertificates returns no error")
		assert.True(t, count > 0, "certificate count is larger than zero")
	})
	t.Run("SSL: List all SSL Certificates", func(t *testing.T) {
		listing, count, err := clientAuto.ListSSLCertificates(EmptyCertificateFilter)
		assert.NoError(t, err, "ListSSLCertificates returns no error")
		assert.True(t, len(listing) > 0, "certificate listing is not empty")
		assert.True(t, count > 0, "certificate count header is bigger than zero")
	})
	t.Run("SSL: List issued SSL Certificates", func(t *testing.T) {
		filter := NewCommonCertificateFilter().Status(CertificateFilterStatusIssued)
		listing, count, err := clientAuto.ListSSLCertificates(filter)
		assert.NoError(t, err, "ListSSLCertificates returns no error")
		assert.True(t, len(listing) > 0, "certificate listing is not empty")
		assert.True(t, count > 0, "certificate count header is bigger than zero")
		// remember first issued certificate ID
		collectedTestData.ExistingCertificateID = listing[0].SSLID
	})
	t.Run("SSL: Show certificate details", func(t *testing.T) {
		details, err := clientAuto.GetSSLCertificateDetails(collectedTestData.ExistingCertificateID)
		assert.NoError(t, err, "GetSSLCertificateDetails returns no error")
		assert.True(t, len(details.SerialNumber) > 0, "certificate has serial number")
	})
	t.Run("SSL: Enroll certificate (RSA 2048)", func(t *testing.T) {
		if skipCertificateCreation {
			t.Skip()
		}
		// create test request
		pkey, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			t.Fatal(err)
		}
		x509req, err := x509.CreateCertificateRequest(rand.Reader, &x509.CertificateRequest{
			SignatureAlgorithm: x509.SHA256WithRSA,
			Subject: pkix.Name{
				CommonName: "rsa-2048.sectigo.tcs.unittest.ca.kit.edu",
			},
		}, pkey)
		if err != nil {
			t.Fatal(err)
		}

		requestParams := EnrollSSLCertificateRequest{
			OrgID: orgID,
			CSR: string(pem.EncodeToMemory(&pem.Block{
				Type:  "NEW CERTIFICATE REQUEST",
				Bytes: x509req,
			})),
			SubjAltNames:      "",
			CertType:          collectedTestData.RSAProfile.ID,
			Term:              collectedTestData.RSAProfile.Terms[0],
			Comments:          "Unit Test for go-sectigo-api-client",
			CustomFields:      nil,
			ExternalRequester: "",
		}
		response, err := clientNoAuto.EnrollSSLCertificate(requestParams)
		assert.NoError(t, err, "EnrollSSLCertificate returns no error")
		collectedTestData.CleanupDeclineCertificateIDs = append(collectedTestData.CleanupDeclineCertificateIDs, response.SSLID)
	})
	t.Run("SSL: Enroll certificate (EC P-384) with SAN", func(t *testing.T) {
		if skipCertificateCreation {
			t.Skip()
		}
		// create test request
		pkey, err := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
		if err != nil {
			t.Fatal(err)
		}
		x509req, err := x509.CreateCertificateRequest(rand.Reader, &x509.CertificateRequest{
			SignatureAlgorithm: x509.ECDSAWithSHA256,
			Subject: pkix.Name{
				CommonName: "ecc-p-384.sectigo.tcs.unittest.ca.kit.edu",
			},
		}, pkey)
		if err != nil {
			t.Fatal(err)
		}

		requestParams := EnrollSSLCertificateRequest{
			OrgID: orgID,
			CSR: string(pem.EncodeToMemory(&pem.Block{
				Type:  "NEW CERTIFICATE REQUEST",
				Bytes: x509req,
			})),
			SubjAltNames:      "ecdsa-p-384.sectigo.tcs.unittest.ca.kit.edu",
			CertType:          collectedTestData.ECCProfile.ID,
			Term:              collectedTestData.ECCProfile.Terms[0],
			Comments:          "Unit Test for go-sectigo-api-client",
			CustomFields:      nil,
			ExternalRequester: "",
		}
		response, err := clientNoAuto.EnrollSSLCertificate(requestParams)
		assert.NoError(t, err, "EnrollSSLCertificate returns no error")
		collectedTestData.CleanupDeclineCertificateIDs = append(collectedTestData.CleanupDeclineCertificateIDs, response.SSLID)
	})
	t.Run("SSL: Collect SSL Certificate (all formats w/o private key)", func(t *testing.T) {
		//t.Skip()
		//collectedTestData.ExistingCertificateID = 2910781 // GÉANT IGTF Multi-Domain
		for _, format := range CollectSSLCertificatePublicKeyList {
			c, err := clientAuto.CollectSSLCertificate(collectedTestData.ExistingCertificateID, format)
			assert.NoError(t, err, "CollectSSLCertificate returns no error")
			assert.True(t, len(c) > 0, "certificate is not empty")
			//assert.NoError(t, ioutil.WriteFile(fmt.Sprintf("/tmp/sectigo_cert_%s.output", format), c, 0644))
			//litter.Dump(format, string(c))
		}
	})
	t.Run("SSL: Decline requests", func(t *testing.T) {
		if skipCertificateCreation {
			t.Skip()
		}
		for _, id := range collectedTestData.CleanupDeclineCertificateIDs {
			err := clientAuto.DeclineSSLCertificate(id, "Removed after unit test")
			assert.NoError(t, err, "DeclineSSLCertificate returns no error")
		}
	})
	t.Run("SSL: Modify request", func(t *testing.T) {
		// look for existing request
		const CertificateToUpdateCommonname = "xn---modifyme-uk85hlsh2qb.sectigo.tcs.unittest.ca.kit.edu"
		specialrequests, num, err := clientNoAuto.ListSSLCertificates(NewCommonCertificateFilter().
			CommonName(CertificateToUpdateCommonname).
			Status(CertificateFilterStatusRequested))
		if err != nil {
			t.Fatal(err)
		}
		var requestId int
		if num < 1 {
			// make a new request
			pkey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
			if err != nil {
				t.Fatal(err)
			}
			x509req, err := x509.CreateCertificateRequest(rand.Reader, &x509.CertificateRequest{
				SignatureAlgorithm: x509.ECDSAWithSHA256,
				Subject: pkix.Name{
					CommonName: CertificateToUpdateCommonname,
				},
			}, pkey)
			if err != nil {
				t.Fatal(err)
			}

			requestParams := EnrollSSLCertificateRequest{
				OrgID: orgID,
				CSR: string(pem.EncodeToMemory(&pem.Block{
					Type:  "NEW CERTIFICATE REQUEST",
					Bytes: x509req,
				})),
				CertType:          collectedTestData.ECCProfile.ID,
				Term:              collectedTestData.ECCProfile.Terms[0],
				Comments:          "Unit Test for go-sectigo-api-client",
				CustomFields:      nil,
				ExternalRequester: "",
			}
			response, err := clientNoAuto.EnrollSSLCertificate(requestParams)
			if err != nil {
				t.Fatal(err)
			}
			requestId = response.SSLID
		} else {
			requestId = specialrequests[0].SSLID
		}
		// Modify request
		newComment := fmt.Sprintf("💣 Updated %s", time.Now().Format(time.RFC3339Nano))
		resp, err := clientNoAuto.UpdateSSLCertificate(UpdateCertificateRequest{
			SSLID:    requestId,
			Comments: newComment,
		})
		assert.NoError(t, err, "UpdateSSLCertificate returns no error")
		assert.Equal(t, resp.Comments, newComment, "new comment equals requested comment")
		t.Logf("New comment for request »%s«: »%s«\n", CertificateToUpdateCommonname, newComment)
	})
}

func TestClientCertificates(t *testing.T) {
	t.Run("Client: List Client Certificate Types", func(t *testing.T) {
		types, err := clientAuto.ListClientCertificateTypes()
		assert.NoError(t, err, "Listing Client Certificate Types returns no errors")
		assert.NotEmpty(t, types, "Got more than zero Client Certificate Types")
		collectedTestData.ClientCertificateTypes = *types
		//litter.Dump(types)
	})
	t.Run("Client: List Client certificates by person email", func(t *testing.T) {
		//const testEmail = "beate.beispiel@kit.edu"
		const testEmail = "heiko.reese@kit.edu"
		certs, err := clientAuto.ListClientCertificateListByPersonEmail(testEmail)
		assert.NoError(t, err, "")
		litter.Dump(certs)
	})
	t.Run("Client: Enroll client certificate (RSA 2048)", func(t *testing.T) {
		if skipCertificateCreation {
			t.Skip()
		}
		if len(collectedTestData.ClientCertificateTypes) < 1 {
			t.Fail()
		}
		id, terms, err := collectedTestData.ClientCertificateTypes.FindByName("GÉANT Personal Certificate")
		assert.NoError(t, err, "Type exists")

		// create test request
		pkey, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			t.Fatal(err)
		}
		x509req, err := x509.CreateCertificateRequest(rand.Reader, &x509.CertificateRequest{
			SignatureAlgorithm: x509.SHA256WithRSA,
			//Subject: pkix.Name{
			//	CommonName: "Beate Beispiel",
			//},
			//EmailAddresses: []string{"beate.beispiel@kit.edu"},
		}, pkey)
		if err != nil {
			t.Fatal(err)
		}

		requestParams := EnrollClientCertificateRequest{
			OrgID: orgID,
			CSR: string(pem.EncodeToMemory(&pem.Block{
				Type:  "NEW CERTIFICATE REQUEST",
				Bytes: x509req,
			})),
			CertType:        id,
			Term:            terms[0],
			Email:           "beate.beispiel@kit.edu",
			Phone:           "",
			SecondaryEmails: nil,
			FirstName:       "Beate",
			MiddleName:      "",
			LastName:        "Beispiel",
			CustomFields:    nil,
		}

		j, _ := json.Marshal(requestParams)
		t.Log(string(j))

		response, err := clientNoAuto.EnrollClientCertificate(requestParams)
		assert.NoError(t, err, "EnrollClientCertificateRequest returns no error")
		assert.NotEmpty(t, response, "EnrollClientCertificateRequest returns an order ID")
		collectedTestData.CleanupDeclineClientCertificateOrderNumbers = append(collectedTestData.CleanupDeclineClientCertificateOrderNumbers, response.OrderNumber)
	})
	t.Run("Client: Collect client certificate", func(t *testing.T) {
		// TODO: write test from actual data
		const (
			orderNumber      = 1701963167
			LeafSerialString = "129225209130958388802841797270959356378"
		)

		response, err := clientNoAuto.CollectClientCertificate(orderNumber)
		assert.NoError(t, err, "CollectClientCertificate returns no error")
		assert.EqualValues(t, LeafSerialString, response[0].SerialNumber.String(), "serial number matches")
	})
	t.Run("Client: Get report on client certificates", func(t *testing.T) {
		reportRequest := ReportClientCertificatesRequest{
			OrganizationIDs: []int{28423},
		}
		response, err := clientAuto.APIReportClientCertificates(reportRequest)
		assert.NoError(t, err, "APIReportClientCertificates returns no error")
		assert.EqualValues(t, 0, response.StatusCode, "statusCode is 0")
		assert.Greater(t, len(response.Reports), 0, "reports are not empty")
	})
}

func TestClientDCV(t *testing.T) {
	t.Run("Domains: Marshal DomainCertTypes", func(t *testing.T) {
		jsonbytes, err := json.Marshal(DomainCertTypes{
			SSL:      true,
			SMIME:    true,
			CodeSign: false,
		})
		assert.NoError(t, err, "json.Marshal() of DomainCertTypes returns no error")
		assert.Equal(t, `["SSL","SMIME"]`, string(jsonbytes), "json.Marshal() of DomainCertTypes is correct")
		jsonbytes, err = json.Marshal(NewDomainRequest{
			Name:        "uni-beispiel.de",
			Description: "Unit testing",
			Active:      true,
			Delegations: []struct {
				OrgID     int             `json:"orgId,omitempty"`
				CertTypes DomainCertTypes `json:"certTypes"`
			}{
				{123456,
					DomainCertTypes{
						SSL:      false,
						SMIME:    false,
						CodeSign: true,
					}},
			},
		})
		assert.NoError(t, err, "json.Marshal() of NewDomainRequest returns no error")
		assert.Equal(t, `{"name":"uni-beispiel.de","description":"Unit testing","active":true,"delegations":[{"orgId":123456,"certTypes":["CodeSign"]}]}`, string(jsonbytes))
		fmt.Println(string(jsonbytes))
	})
}

func TestListClientCertificates(t *testing.T) {
	certs, err := clientAuto.GetClientCertificateListing(ClientCertificateListingFilter{})
	assert.NoError(t, err, "GetClientCertificateListing returns no error")
	assert.NotEmpty(t, certs, "GetClientCertificateListing returns certificates")
	litter.Dump(certs)
}

func TestDomains(t *testing.T) {
	t.Run("Count number of domains", func(t *testing.T) {
		count, err := clientAuto.CountDomains()
		assert.NoError(t, err, "CountDomains returns no error")
		assert.Greater(t, count, 0, "CountDomains returns a count greater than 0")
	})
	t.Run("Domains: List all domains", func(t *testing.T) {
		domains, err := clientAuto.ListDomains()
		assert.NoError(t, err, "ListDomains returns no error")
		assert.NotEmpty(t, domains, "ListDomains returns domains")
	})
	//t.Run("Domains: List all domains with filter", func(t *testing.T) {
	//	domains, err := clientAuto.ListDomains()
	//	assert.NoError(t, err, "ListDomains returns no error")
	//	assert.NotEmpty(t, domains, "ListDomains returns domains")
	//})
}
