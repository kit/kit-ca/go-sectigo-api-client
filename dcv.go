package sectigoapiclient

import (
	"bytes"
	"encoding/json"
	"io"
	"net/url"
	"strconv"
)

type DCVRequest struct {
	Domain string `json:"domain"`
}

func (r DCVRequest) Reader() (io.Reader, error) {
	jsonbuf, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(jsonbuf), nil
}

type DCVHTTPResponse struct {
	URL        string `json:"url"`
	FirstLine  string `json:"firstLine"`
	SecondLine string `json:"secondLine"`
}

type DCVHTTPSResponse DCVHTTPResponse

type DCVCNAMEResponse struct {
	Host  string `json:"host"`
	Point string `json:"point"`
}

type DCVEMailResponse struct {
	Emails []string `json:"emails"`
}

type DCVSubmitValidationResponse struct {
	OrderStatus string `json:"orderStatus"`
	Message     string `json:"message"`
	Status      string `json:"status"`
}

type DCVValidationStatusResponse struct {
	Status         string `json:"status"`
	OrderStatus    string `json:"orderStatus"`
	ExpirationDate string `json:"expirationDate"`
}

type DCVDomainSearchResponse []struct {
	Domain         string `json:"domain"`
	DCVStatus      string `json:"dcvStatus"`
	DCVOrderStatus string `json:"dcvOrderStatus"`
	DCVMethod      string `json:"dcvMethod"`
	ExpirationDate string `json:"expirationDate"`
}

type DCVClearValidationResponse DCVSubmitValidationResponse

func (c *SectigoClient) DCVStartValidationHTTP(req DCVRequest) (response DCVHTTPResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVHTTPResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVStartHTTP, "", bodyReader)
	if err != nil {
		return DCVHTTPResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVHTTPResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVHTTPResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) DCVStartValidationHTTPS(req DCVRequest) (response DCVHTTPSResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVHTTPSResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVStartHTTPS, "", bodyReader)
	if err != nil {
		return DCVHTTPSResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVHTTPSResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVHTTPSResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) DCVStartValidationCNAME(req DCVRequest) (response DCVCNAMEResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVCNAMEResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVStartHTTPS, "", bodyReader)
	if err != nil {
		return DCVCNAMEResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVCNAMEResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVCNAMEResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) DCVStartValidationEMail(req DCVRequest) (response DCVEMailResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVEMailResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVStartEMail, "", bodyReader)
	if err != nil {
		return DCVEMailResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVEMailResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVEMailResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) DCVSubmitValidationHTTP(req DCVRequest) (response DCVSubmitValidationResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVSubmitValidationHTTP, "", bodyReader)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) DCVSubmitValidationHTTPS(req DCVRequest) (response DCVSubmitValidationResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVSubmitValidationHTTPS, "", bodyReader)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) DCVSubmitValidationCNAME(req DCVRequest) (response DCVSubmitValidationResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVSubmitValidationCNAME, "", bodyReader)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) DCVSubmitValidationEMail(req DCVRequest) (response DCVSubmitValidationResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVSubmitValidationEMail, "", bodyReader)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVSubmitValidationResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) ApiRequestDCVGetValidationStatus(req DCVRequest) (response DCVValidationStatusResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVValidationStatusResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVGetValidationStatus, "", bodyReader)
	if err != nil {
		return DCVValidationStatusResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVValidationStatusResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVValidationStatusResponse{}, err
	}
	return response, nil
}

func (c *SectigoClient) ApiRequestDCVClearValidation(req DCVRequest) (response DCVClearValidationResponse, err error) {
	bodyReader, err := req.Reader()
	if err != nil {
		return DCVClearValidationResponse{}, err
	}
	resp, err := c.doRequest(ApiRequestDCVClearValidation, "", bodyReader)
	if err != nil {
		return DCVClearValidationResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVClearValidationResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVClearValidationResponse{}, err
	}
	return response, nil
}

type DCVSearchDomainsFilter struct {
	url.Values
}

func NewDCVSearchDomainsRequest() DCVSearchDomainsFilter {
	return DCVSearchDomainsFilter{make(url.Values)}
}

func (r DCVSearchDomainsFilter) AsURLParameterString() string {
	return r.Encode()
}

func (r DCVSearchDomainsFilter) Size(arg int) DCVSearchDomainsFilter {
	r.Add("size", strconv.Itoa(arg))
	return r
}

func (r DCVSearchDomainsFilter) Position(arg int) DCVSearchDomainsFilter {
	r.Add("position", strconv.Itoa(arg))
	return r
}

func (r DCVSearchDomainsFilter) Domain(arg string) DCVSearchDomainsFilter {
	r.Add("domain", arg)
	return r
}

func (r DCVSearchDomainsFilter) Org(arg int) DCVSearchDomainsFilter {
	r.Add("org", strconv.Itoa(arg))
	return r
}

func (r DCVSearchDomainsFilter) Department(arg int) DCVSearchDomainsFilter {
	r.Add("department", strconv.Itoa(arg))
	return r
}

func (r DCVSearchDomainsFilter) DCVStatus(arg int) DCVSearchDomainsFilter {
	r.Add("dcvStatus", strconv.Itoa(arg))
	return r
}

func (r DCVSearchDomainsFilter) OrderStatus(arg int) DCVSearchDomainsFilter {
	r.Add("orderStatus", strconv.Itoa(arg))
	return r
}

func (r DCVSearchDomainsFilter) ExpiresIn(arg int) DCVSearchDomainsFilter {
	r.Add("expiresIn", strconv.Itoa(arg))
	return r
}

func (c *SectigoClient) DCVSearchDomains(filter DCVSearchDomainsFilter) (response DCVDomainSearchResponse, err error) {
	resp, err := c.doRequest(ApiRequestDCVSearchDomains, "?"+filter.AsURLParameterString(), nil)
	if err != nil {
		return DCVDomainSearchResponse{}, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return DCVDomainSearchResponse{}, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return DCVDomainSearchResponse{}, err
	}
	return response, nil
}
