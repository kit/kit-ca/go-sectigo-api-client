package sectigoapiclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// ClientCertificateListingFilter is a filter for the client certificate listing
type ClientCertificateListingFilter struct {
	values url.Values
}

func NewClientCertificateListingFilter() *ClientCertificateListingFilter {
	return &ClientCertificateListingFilter{}
}

func (f *ClientCertificateListingFilter) SetSize(size int) *ClientCertificateListingFilter {
	if f.values == nil { // lazy init
		f.values = make(url.Values)
	}
	f.values.Set("size", strconv.Itoa(size))
	return f
}

func (f *ClientCertificateListingFilter) SetPosition(offset int) *ClientCertificateListingFilter {
	if f.values == nil { // lazy init
		f.values = make(url.Values)
	}
	f.values.Set("position", strconv.Itoa(offset))
	return f
}

// ByPersonID adds a filter for the person ID
func (f *ClientCertificateListingFilter) ByPersonID(id int) *ClientCertificateListingFilter {
	if f.values == nil { // lazy init
		f.values = make(url.Values)
	}
	f.values.Set("personId", strconv.Itoa(id))
	return f
}

// ByState adds a filter for the certificate state
func (f *ClientCertificateListingFilter) ByState(state CertificateState) *ClientCertificateListingFilter {
	if f.values == nil { // lazy init
		f.values = make(url.Values)
	}
	f.values.Set("state", strings.ToLower(state.String()))
	return f
}

// ByBackendCertID adds a filter for the backend certificate ID
func (f *ClientCertificateListingFilter) ByBackendCertID(id string) *ClientCertificateListingFilter {
	if f.values == nil { // lazy init
		f.values = make(url.Values)
	}
	f.values.Set("backendCertId", id)
	return f
}

func (f *ClientCertificateListingFilter) String() string {
	if f.values == nil { // lazy init
		f.values = make(url.Values)
	}
	return "?" + f.values.Encode()
}

// ClientCertificateListing is a response to a client certificates listing request
type ClientCertificateListing []struct {
	ID                 int `json:"id"`
	CertificateDetails struct {
		Issuer          string `json:"issuer"`
		Subject         string `json:"subject"`
		SubjectAltNames string `json:"subjectAltNames"`
		MD5Hash         string `json:"md5Hash"`
		SHA1Hash        string `json:"sha1Hash"`
	} `json:"certificateDetails"`
	State         string                   `json:"state"`
	OrderNumber   int                      `json:"orderNumber"`
	SerialNumber  string                   `json:"serialNumber"`
	BackendCertID string                   `json:"backendCertId"`
	Expires       *DateTimestampFromString `json:"expires"`
}

func (c *SectigoClient) GetClientCertificateListing(filter ClientCertificateListingFilter) (list ClientCertificateListing, err error) {
	const batchSize = 200
	var (
		resp *http.Response
	)

	// get total count
	resp, err = c.doRequest(ApiRequestCountClientCertificates, filter.String(), nil)
	if err != nil {
		return nil, err
	}
	countString := resp.Header.Get("X-Total-Count")
	if countString == "" {
		return nil, errors.New("No certificates found that match the filter")
	}
	count, err := strconv.Atoi(countString)
	if err != nil {
		return nil, err
	}

	// iterate over all paged answers
	for offset := 0; offset < count; offset += batchSize {
		filter.SetSize(batchSize).SetPosition(offset)
		resp, err = c.doRequest(ApiRequestListClientCertificates, filter.String(), nil)
		if err != nil {
			return nil, err
		}

		buf := new(bytes.Buffer)
		_, err = buf.ReadFrom(resp.Body)
		if err != nil {
			return nil, err
		}
		response := new(ClientCertificateListing)
		err = json.Unmarshal(buf.Bytes(), &response)
		if err != nil {
			return nil, err
		}
		list = append(list, *response...)
	}
	return list, nil
}
