package sectigoapiclient

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"time"
)

type loggingTransport http.Transport

// RoundTrip logs request and response (header and body) to stderr
func (s *loggingTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	commBytes, _ := httputil.DumpRequestOut(r, true)

	// visually separate request and response
	commBytes = append(commBytes, []byte("\n----------\n\n")...)

	resp, err := http.DefaultTransport.RoundTrip(r)
	// err is returned after dumping the response

	respBytes, _ := httputil.DumpResponse(resp, true)
	commBytes = append(commBytes, respBytes...)

	fmt.Fprintf(os.Stderr, "%s\n", commBytes)

	return resp, err
}

var defaultHTTPClient = http.Client{}

func init() {
	_, debugDumpHttp := os.LookupEnv("DEBUG_DUMP_HTTP")

	defaultHTTPClient.CheckRedirect = nil
	defaultHTTPClient.Timeout = 60 * time.Second

	// use request logging client if DEBUG_DUMP_HTTP is set
	if debugDumpHttp {
		defaultHTTPClient.Transport = &loggingTransport{ // logging client
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
			IdleConnTimeout:     90 * time.Second,
			TLSHandshakeTimeout: 10 * time.Second,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: false,
				Renegotiation:      tls.RenegotiateOnceAsClient,
			},
		}
	} else {
		defaultHTTPClient.Transport = &http.Transport{ // normal client
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
			IdleConnTimeout:     90 * time.Second,
			TLSHandshakeTimeout: 10 * time.Second,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: false,
				Renegotiation:      tls.RenegotiateOnceAsClient,
			},
		}
	}
}

// GetCredentials return a username and a password
type GetCredentials func() (username string, password string)

// InjectAuthHeaders adds all headers to http.Request for client auth
type InjectAuthHeaders func(req *http.Request)

// SectigoClient is a modified http.Client that authenticates against Sectigo's API
type SectigoClient struct {
	*http.Client
	AuthFunc InjectAuthHeaders
}

// NewPasswordClient creates a new Sectigo API client that uses username/password for auth
func NewPasswordClient(getCreds GetCredentials, customerUri string) *SectigoClient {
	login, password := getCreds()

	return &SectigoClient{&defaultHTTPClient, func(req *http.Request) {
		req.Header.Add("login", login)
		req.Header.Add("password", password)
		req.Header.Add("customerUri", customerUri)
	}}
}

// SectigoApiError represents an API error as specified in the API spec
type SectigoApiError struct {
	Code        int
	Description string
}

func (e SectigoApiError) Error() string {
	return e.Description
}

// HTTPError is a fallback type for HTTP errors
type HTTPError struct {
	Returncode  int
	Description string
}

func (e HTTPError) Error() string {
	return fmt.Sprintf("HTTP Error %d: %s", e.Returncode, e.Description)
}

// doRequest performs an API call and handles errors on a basic level
func (c *SectigoClient) doRequest(request APIRequest, pathArgument string, body io.Reader) (response *http.Response, err error) {
	// get request config
	config, exists := ApiRequests[request]
	if !exists {
		return nil, errors.New("No configuration for " + request.String())
	}

	// build request
	req, err := http.NewRequest(config.Verb, SECTIGOAPIURL+config.PathPrefix+pathArgument, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json;charset=utf-8")
	// add auth headers
	c.AuthFunc(req)

	// perform request
	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	switch code := resp.StatusCode; {
	// success
	case code >= 200 && code < 300:
		return resp, nil
	// Bad Request
	case code == 400:
		var (
			apiError SectigoApiError
			buf      = new(bytes.Buffer)
		)
		_, err = buf.ReadFrom(resp.Body)
		if err != nil {
			return resp, err
		}
		err = json.Unmarshal(buf.Bytes(), &apiError)
		if err != nil {
			return resp, err
		}
		return resp, apiError
	// Unauthorized
	// TODO: evtl body parsen?
	case code == 401:
		return resp, HTTPError{
			Returncode:  code,
			Description: "Unauthorized",
		}
	// Not found
	case code == 404:
		return resp, HTTPError{
			Returncode:  code,
			Description: "Not Found",
		}
	// Everything else
	default:
		return resp, HTTPError{
			Returncode:  code,
			Description: "Unknown",
		}
	}
}
