module gitlab.kit.edu/kit/kit-ca/go-sectigo-api-client

go 1.18

require (
	github.com/sanity-io/litter v1.5.5
	github.com/stretchr/testify v1.8.4
	go.mozilla.org/pkcs7 v0.0.0-20210826202110-33d05740a352
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jackc/pgx v3.6.2+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
