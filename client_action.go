package sectigoapiclient

import (
	"bytes"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"go.mozilla.org/pkcs7"
	"io"
	"net/url"
	"strconv"
)

type ClientCertificateTypeResponse []struct {
	Name        string `json:"Name"`
	Description string `json:"description,omitempty"`
	ID          int    `json:"ID"`
	Terms       []int  `json:"Terms"`
	KeyTypes    struct {
		EC  []string
		RSA []string
	} `json:"keyTypes,omitempty"`
	UseSecondaryOrgName bool `json:"useSecondaryOrgName,omitempty"`
}

func (types ClientCertificateTypeResponse) FindByName(name string) (id int, terms []int, err error) {
	for _, CertType := range types {
		if CertType.Name == name {
			return CertType.ID, CertType.Terms, nil
		}
	}
	return -1, nil, errors.New("unable to find matching type")
}

func (c *SectigoClient) ListClientCertificateTypes() (response *ClientCertificateTypeResponse, err error) {
	resp, err := c.doRequest(ApiRequestClientCertificateTypes, "", nil)
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

type EnrollClientCertificateRequest struct {
	OrgID           int      `json:"orgId"`
	CSR             string   `json:"csr"`
	CertType        int      `json:"certType"`
	Term            int      `json:"term"`
	Email           string   `json:"email"`
	Phone           string   `json:"phone"`
	SecondaryEmails []string `json:"secondaryEmails"`
	FirstName       string   `json:"firstName"`
	MiddleName      string   `json:"middleName"`
	LastName        string   `json:"lastName"`
	CustomFields    []struct {
		Name  string `json:"name,omitempty"`
		Value string `json:"value,omitempty"`
	} `json:"customFields,omitempty"`
}

func (f EnrollClientCertificateRequest) Reader() (io.Reader, error) {
	jsonbuf, err := json.Marshal(f)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(jsonbuf), nil
}

type ClientCertificateEnrollmentResponse struct {
	OrderNumber int `json:"orderNumber"`
	//BackendCertId string `json:"backendCertId,omitempty"`
}

func (c *SectigoClient) EnrollClientCertificate(certRequest EnrollClientCertificateRequest) (response *ClientCertificateEnrollmentResponse, err error) {
	response = new(ClientCertificateEnrollmentResponse)
	fr, err := certRequest.Reader()
	if err != nil {
		return nil, err
	}
	resp, err := c.doRequest(ApiRequestEnrollClientCertificate, "", fr)
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// Collect Client certificate
// https://www.sectigo.com/uploads/audio/Certificate-Manager-20.1-Rest-API.html#resource-SMIME-collect

type ClientCertificateCollectResponse struct {
	OrderNumber int `json:"orderNumber"`
}

func (c *SectigoClient) CollectClientCertificate(orderNumber int) (certificates []*x509.Certificate, err error) {
	// do request
	resp, err := c.doRequest(ApiRequestCollectClientCertificate, strconv.Itoa(orderNumber), nil)
	if err != nil {
		return nil, err
	}

	// read body into buffer
	buf := new(bytes.Buffer)
	p7PEM, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}

	// decode PEM
	block, rest := pem.Decode(p7PEM)
	if len(rest) > 0 {
		return nil, errors.New("found trailing data while decoding PEM")
	}

	// decode PKCS7
	p7, err := pkcs7.Parse(block.Bytes)
	if err != nil {
		return nil, err
	}
	return p7.Certificates, nil
}

// List Client certificates by person email
// https://www.sectigo.com/uploads/audio/Certificate-Manager-20.1-Rest-API.html#resource-SMIME-by-person-email

type ClientCertificateListByPersonEmail []struct {
	ID                 int    `json:"id"`
	Subject            string `json:"subject"`
	State              string `json:"state"`
	OrderNumber        int    `json:"orderNumber"`
	SerialNumber       string `json:"serialNumber"`
	Expires            string `json:",omitempty"` // not in API docs
	CertificateDetails struct {
		Issuer          string `json:"issuer,omitempty"`          // not in API docs
		Subject         string `json:"subject,omitempty"`         // not in API docs
		SubjectAltNames string `json:"subjectAltNames,omitempty"` // not in API docs
		MD5Hash         string `json:"md5Hash,omitempty"`         // not in API docs
		SHA1Hash        string `json:"sha1Hash,omitempty"`        // not in API docs
	} `json:"certificateDetails,omitempty"` // not in API docs
}

func (c *SectigoClient) ListClientCertificateListByPersonEmail(email string) (list ClientCertificateListByPersonEmail, err error) {
	response := new(ClientCertificateListByPersonEmail)

	resp, err := c.doRequest(ApiRequestListClientCertificateByEmail, url.QueryEscape(email), nil)
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return nil, err
	}
	return *response, nil
}
