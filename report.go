package sectigoapiclient

import (
	"bytes"
	"encoding/json"
	"io"
	"time"
)

const (
	APIReportCertificateStatusAny                     = 0
	APIReportCertificateStatusRequested               = 1
	APIReportCertificateStatusIssued                  = 2
	APIReportCertificateStatusRevoked                 = 3
	APIReportCertificateStatusExpired                 = 4
	APIReportCertificateStatusEnrolledPendingDownload = 5
	APIReportCertificateStatusNotEnrolled             = 6
	APIReportCertificateStatusAwaitingApproval        = 7 // Deprecated: falls back to "Requested". "Requested" status should be used instead
	APIReportCertificateStatusApproved                = 8
	APIReportCertificateStatusApplied                 = 9
	APIReportCertificateStatusDownloaded              = 10 // Deprecated: Issued with "certificateDateAttribute" equal to "Date of Downloading" should be used instead
	APIReportCertificateStatusExternal                = 11 // Deprecated: falls back to Issued. Issued with "certificateRequestSource" should be used instead

	APIReportDateAttributeTypeCodeDateOfEnrollment  = 0
	APIReportDateAttributeTypeCodeDateOfDownloading = 1
	APIReportDateAttributeTypeCodeDateOfRevocation  = 2
	APIReportDateAttributeTypeCodeDateOfExpiration  = 3
	APIReportDateAttributeTypeCodeDateOfRequest     = 4
	APIReportDateAttributeTypeCodeDateOfIssuance    = 5
	APIReportDateAttributeTypeCodeDateOfInvitation  = 6
)

type ReportClientCertificatesRequest struct {
	OrganizationIDs          []int  `json:"organizationIds"`
	CertificateStatus        int    `json:"certificateStatus,omitempty"`        // Use APIReportCertificateStatus* constants
	CertificateDateAttribute int    `json:"certificateDateAttribute,omitempty"` // Use APIReportDateAttributeTypeCodeDateO* constants
	From                     string `json:"from,omitempty"`
	To                       string `json:"to,omitempty"`
}

func (req *ReportClientCertificatesRequest) SetFrom(timestamp time.Time) *ReportClientCertificatesRequest {
	req.From = timestamp.Format("2006-01-02")
	return req
}

func (req *ReportClientCertificatesRequest) SetTo(timestamp time.Time) *ReportClientCertificatesRequest {
	req.To = timestamp.Format("2006-01-02")
	return req
}

func (req *ReportClientCertificatesRequest) Reader() (io.Reader, error) {
	jsonBuf, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(jsonBuf), nil
}

type ReportClientCertificatesResponse struct {
	StatusCode int `json:"status_code"`
	Reports    []struct {
		ID            int        `json:"id"`
		Subject       string     `json:"subject"`
		Email         string     `json:"email"`
		OrderNumber   *uint64    `json:"orderNumber"` // Deprecated: Use backendCertId
		BackendCertID *string    `json:"backendCertId"`
		Enrolled      *time.Time `json:"enrolled"`
		Downloaded    *time.Time `json:"downloaded"`
		Expire        *time.Time `json:"expire"`
		Revoked       *time.Time `json:"revoked,omitempty"` // not in API documentation; missing if not revoked
		EnrollType    string     `json:"enrollType"`
		Organization  struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"organization"`
		Person struct {
			Name  string  `json:"name"`
			Email string  `json:"email"`
			GUID  *string `json:"guid"`
		} `json:"person"`
		CustomFields []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"customFields,omitempty"`
	} `json:"reports"`
}

func (c *SectigoClient) APIReportClientCertificates(req ReportClientCertificatesRequest) (response *ReportClientCertificatesResponse, err error) {
	requestBodyReader, err := req.Reader()
	if err != nil {
		return nil, err
	}
	resp, err := c.doRequest(ApiRequestReportClientCertificates, "", requestBodyReader)
	if err != nil {
		return response, err
	}
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return response, err
	}
	err = json.Unmarshal(buf.Bytes(), &response)
	if err != nil {
		return response, err
	}
	return response, nil
}
