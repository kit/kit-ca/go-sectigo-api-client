package sectigoapiclient

import (
	"strconv"
	"time"
)

// IntString is a type that can be unmarshalled from a JSON string to an integer
type IntString int

// UnmarshalJSON for IntString
func (is *IntString) UnmarshalJSON(b []byte) error {
	// Remove quotes from the JSON value to convert it from string to int
	unquoted, err := strconv.Unquote(string(b))
	if err != nil {
		return err
	}

	// Convert the unquoted string to an integer
	intValue, err := strconv.Atoi(unquoted)
	if err != nil {
		return err
	}

	// Assign the converted integer value to the IntString field
	*is = IntString(intValue)
	return nil
}

type DateTimestampFromString time.Time

// UnmarshalJSON for DateTimestampFromString
func (ds *DateTimestampFromString) UnmarshalJSON(b []byte) error {
	// Remove quotes from the JSON value to convert it from string to int
	unquoted, err := strconv.Unquote(string(b))
	if err != nil {
		return err
	}
	t, err := time.Parse("2006-01-02", unquoted)
	if err != nil {
		return err
	}
	*ds = DateTimestampFromString(t)
	return nil
}
