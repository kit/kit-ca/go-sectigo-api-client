package sectigoapiclient

import (
	"net/url"
	"strconv"
)

var (
	EmptyCertificateFilter = NewCommonCertificateFilter()
)

type CommonCertificateFilter struct {
	url.Values
}

func NewCommonCertificateFilter() CommonCertificateFilter {
	return CommonCertificateFilter{make(url.Values)}
}

func (f CommonCertificateFilter) AsURLParameterString() string {
	return f.Encode()
}

func (f CommonCertificateFilter) Size(arg int) CommonCertificateFilter {
	f.Add("size", strconv.Itoa(arg))
	return f
}

func (f CommonCertificateFilter) Position(arg int) CommonCertificateFilter {
	f.Add("position", strconv.Itoa(arg))
	return f
}

func (f CommonCertificateFilter) CommonName(arg string) CommonCertificateFilter {
	f.Add("commonName", arg)
	return f
}

func (f CommonCertificateFilter) SubjectAlternativeName(arg string) CommonCertificateFilter {
	f.Add("subjectAlternativeName", arg)
	return f
}

//go:generate stringer -type=CertificateFilterStatus -trimprefix=CertificateFilterStatus -output zzz_CertificateFilterStatus_string.go
type CertificateFilterStatus int

const (
	CertificateFilterStatusInvalid CertificateFilterStatus = 1 + iota
	CertificateFilterStatusRequested
	CertificateFilterStatusApproved
	CertificateFilterStatusDeclined
	CertificateFilterStatusApplied
	CertificateFilterStatusIssued
	CertificateFilterStatusRevoked
	CertificateFilterStatusExpired
	CertificateFilterStatusReplaced
	CertificateFilterStatusRejected
	CertificateFilterStatusUnmanaged
	CertificateFilterStatusSAApproved
	CertificateFilterStatusInit
)

func (f CommonCertificateFilter) Status(arg CertificateFilterStatus) CommonCertificateFilter {
	f.Add("status", arg.String())
	return f
}

func (f CommonCertificateFilter) SSLTypeID(arg int) CommonCertificateFilter {
	f.Add("sslTypeId", strconv.Itoa(arg))
	return f
}

//go:generate stringer -type=CertificateFilterDiscoveryStatus -trimprefix=CertificateFilterDiscoveryStatus -output zzz_CertificateFilterDiscoveryStatus_string.go
type CertificateFilterDiscoveryStatus int

const (
	CertificateFilterDiscoveryStatusNotDeployed CertificateFilterDiscoveryStatus = 1 + iota
	CertificateFilterDiscoveryStatusDeployed
)

func (f CommonCertificateFilter) DiscoveryStatus(arg CertificateFilterDiscoveryStatus) CommonCertificateFilter {
	f.Add("discoveryStatus", arg.String())
	return f
}

func (f CommonCertificateFilter) Vendor(arg string) CommonCertificateFilter {
	f.Add("vendor", arg)
	return f
}

func (f CommonCertificateFilter) OrgID(arg int) CommonCertificateFilter {
	f.Add("orgId", strconv.Itoa(arg))
	return f
}

//go:generate stringer -type=CommonCertificateFilterInstallStatus -linecomment -output zzz_CommonCertificateFilterInstallStatus_string.go
type CommonCertificateFilterInstallStatus int

const (
	InstallStatusNotScheduled CommonCertificateFilterInstallStatus = 1 + iota // NOT_SCHEDULED
	InstallStatusScheduled                                                    // SCHEDULED
	InstallStatusStarted                                                      // STARTED
	InstallStatusSuccessful                                                   // SUCCESSFUL
	InstallStatusFailed                                                       // FAILED
)

func (f CommonCertificateFilter) installStatus(arg CommonCertificateFilterInstallStatus) CommonCertificateFilter {
	f.Add("installStatus", arg.String())
	return f
}

//go:generate stringer -type=CommonCertificateFilterRenewalStatus -linecomment -output zzz_CommonCertificateFilterRenewalStatus_string.go
type CommonCertificateFilterRenewalStatus int

const (
	RenewalStatusNotScheduled CommonCertificateFilterRenewalStatus = 1 + iota // NOT_SCHEDULED
	RenewalStatusScheduled                                                    // SCHEDULED
	RenewalStatusStarted                                                      // STARTED
	RenewalStatusSuccessful                                                   // SUCCESSFUL
	RenewalStatusFailed                                                       // FAILED
)

func (f CommonCertificateFilter) renewalStatus(arg CommonCertificateFilterRenewalStatus) CommonCertificateFilter {
	f.Add("renewalStatus", arg.String())
	return f
}

func (f CommonCertificateFilter) Issuer(arg string) CommonCertificateFilter {
	f.Add("issuer", arg)
	return f
}

func (f CommonCertificateFilter) SerialNumber(arg string) CommonCertificateFilter {
	f.Add("serialNumber", arg)
	return f
}

func (f CommonCertificateFilter) Requester(arg string) CommonCertificateFilter {
	f.Add("requester", arg)
	return f
}

func (f CommonCertificateFilter) ExternalRequester(arg string) CommonCertificateFilter {
	f.Add("externalRequester", arg)
	return f
}

func (f CommonCertificateFilter) SignatureAlgorithm(arg string) CommonCertificateFilter {
	f.Add("signatureAlgorithm", arg)
	return f
}

func (f CommonCertificateFilter) KeyAlgorithm(arg string) CommonCertificateFilter {
	f.Add("keyAlgorithm", arg)
	return f
}

func (f CommonCertificateFilter) KeyParam(arg string) CommonCertificateFilter {
	f.Add("keyParam", arg)
	return f
}

func (f CommonCertificateFilter) SHA1Hash(arg string) CommonCertificateFilter {
	f.Add("sha1Hash", arg)
	return f
}

func (f CommonCertificateFilter) MD5Hash(arg string) CommonCertificateFilter {
	f.Add("md5Hash", arg)
	return f
}

func (f CommonCertificateFilter) KeyUsage(arg string) CommonCertificateFilter {
	f.Add("keyUsage", arg)
	return f
}

func (f CommonCertificateFilter) ExtendedKeyUsage(arg string) CommonCertificateFilter {
	f.Add("extendedKeyUsage", arg)
	return f
}

//go:generate stringer -type=CommonCertificateFilterRequestedVia -linecomment -output zzz_CommonCertificateFilterRequestedVia_string.go
type CommonCertificateFilterRequestedVia int

const (
	CommonCertificateFilterWebForm     CommonCertificateFilterRequestedVia = 1 + iota // WEB_FORM
	CommonCertificateFilterClientAdmin                                                // CLIENT_ADMIN
	CommonCertificateFilterAPI                                                        // API
	CommonCertificateFilterDiscovery                                                  // DISCOVERY
	CommonCertificateFilterImported                                                   // IMPORTED
	CommonCertificateFilterSCEP                                                       // SCEP
	CommonCertificateFilterCDAgent                                                    // CD_AGENT
	CommonCertificateFilterMSAgent                                                    // MS_AGENT
	CommonCertificateFilterMSCA                                                       // MS_CA
	CommonCertificateFilterBulkRequest                                                // BULK_REQUEST
	CommonCertificateFilterACME                                                       // ACME
	CommonCertificateFilterEST                                                        // EST
	CommonCertificateFilterREST                                                       // REST
)

func (f CommonCertificateFilter) RequestedVia(arg CommonCertificateFilterRequestedVia) CommonCertificateFilter {
	f.Add("requestedVia", arg.String())
	return f
}
